namespace WebAPI.Models;

class Group{
    public int index {get;set;}
    public List<Position> positions {get;set;} = new List<Position>();
}