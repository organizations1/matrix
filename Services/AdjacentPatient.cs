using System.Linq;
using WebAPI.Interfaces;
using WebAPI.Models;

namespace WebAPI.Services;

public class Adjacent :IAdjacent
{
    /**
    For Checking the Adjacent patents.
    */
    public int AdjacentElements<T>(T[,] arr)
    {
        int rows = arr.GetLength(0);
        int columns = arr.GetLength(1);
        List<Group> GroupList = new List<Group>();
        List<Position> PositionList = new List<Position>();

        for(int r = 0 ; r < rows ; r++)
        {
            for(int c = 0; c< columns ; c++)
            {
                if(Convert.ToInt64(arr[r,c]) == 1)
                {
                PositionList = checkDirections(arr,r,c,rows - 1,columns - 1);
                if(PositionList.Count !=0)
                {
                    if(GroupList.Count == 0)
                    {
                        
                        Group grop = new Group();
                        grop.index = 1;
                        grop.positions = PositionList;
                        GroupList.Add(grop);
                        
                    }
                    else
                    {
                        checkPosition(PositionList,GroupList);
                    }
                }
                }
            }
        }

        return GroupList.Count;
    }

    /**
    For checking the patients in all possible directions.
    */
    private List<Position> checkDirections<T>(T[,] arr,int row,int col, int maxRows,int maxColumn)
    {
        List<Position> PositionList = new List<Position>();

            if(row != maxRows && col != maxColumn)
            {
                //Right Up
                if(Convert.ToInt32(arr[row + 1 , col+1]) == 1)
                {
                    Position pos =  new Position();
                    pos.row = row + 1;pos.column = col+1;
                    PositionList.Add(pos);
                }
            }
            if(row != maxRows){
                //Down
                if(Convert.ToInt32(arr[row+1 , col]) == 1)
                {
                    Position pos =  new Position();
                    pos.row = row+1;pos.column = col;
                    PositionList.Add(pos);
                }        
            }
            if(col != maxColumn){
                //Right
                if(Convert.ToInt32(arr[row , col+1]) == 1)
                {
                    Position pos =  new Position();
                    pos.row = row;pos.column = col+1;
                    PositionList.Add(pos);
                }
            }
            
            if(col != 0)
            {
                //Left
                if(Convert.ToInt32(arr[row , col-1]) == 1)
                {
                    Position pos =  new Position();
                    pos.row = row;pos.column = col-1;
                    PositionList.Add(pos);
                }

                if(row != maxRows)
                {
                    //Left Up
                    if(Convert.ToInt32(arr[row+1 , col-1]) == 1)
                    {
                        Position pos =  new Position();
                        pos.row = row + 1;pos.column = col-1;
                        PositionList.Add(pos);
                    } 
                }  
            }
            if(row != 0)
            {
                
                //Up
                if(Convert.ToInt32(arr[row-1 , col]) == 1)
                {
                Position pos =  new Position();
                pos.row = row-1;pos.column = col;
                PositionList.Add(pos);
                }

                if(col != maxColumn)
                {
                    //Right Down
                    if(Convert.ToInt32(arr[row-1 , col+1]) == 1)
                    {
                        Position pos =  new Position();
                        pos.row = row-1;pos.column = col+1;
                        PositionList.Add(pos);
                    }
                }

            }        
        
            if(row != 0 && col != 0)
            {
                //Left Down
                if(Convert.ToInt32(arr[row-1 , col-1]) == 1)
                {
                    Position pos =  new Position();
                    pos.row = row-1;pos.column = col-1;
                    PositionList.Add(pos);
                }
            }

        //Adding Running Position.
        if(PositionList.Count != 0)
        {
            Position pos =  new Position();
            pos.row = row ;pos.column = col;
            PositionList.Add(pos);
        }
        return PositionList;
    }

    /**
    For Checking eithere there is any patient on that Position or not.
    */
    private List<Group> checkPosition(List<Position> pos,List<Group> GroupsList)
    {
        int GroupLength = 0;
        foreach (Group item in GroupsList)
        {
        var result =  item.positions.Where(posi => posi.row == pos[0].row && posi.column == pos[0].column);
        if(result.Count() > 0)
        {
                foreach (Position po in pos)
                {
                    item.positions.Add(po);
                }
        }
        else{
                GroupLength ++;
        }
        }

        if(GroupLength == GroupsList.Count)
        {

            Group grop = new Group();
            grop.index = GroupsList.Max(data => data.index) + 1;
            grop.positions = pos;
            GroupsList.Add(grop);
        }

        return GroupsList;
    }
}