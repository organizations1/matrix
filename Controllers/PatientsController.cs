using Microsoft.AspNetCore.Mvc;
using WebAPI.Interfaces;
using WebAPI.Models;
using System.Text.Json;

namespace WebAPI.Controllers;

[ApiController]
[Route("api/patient-groups")]
public class PatientsController : ControllerBase
{
    private readonly ILogger<PatientsController> _logger;
    private readonly IAdjacent _adjacent;

    public PatientsController(ILogger<PatientsController> logger,IAdjacent adjacent)
    {
        _logger = logger;
        _adjacent = adjacent;
    }

    [HttpPost]
    [Route("calculate")]
    public PatientsResult calculate([FromBody] JsonElement matrix)
    {
       string json = System.Text.Json.JsonSerializer.Serialize(matrix);
       //Matrix array.
       var arr = new[,] { {1,1,0,0,0,0}, {0,1,0,0,0,0}, {1,0,1,0,0,0},{0,0,0,0,1,0},{0,0,0,0,0,1},{1,1,0,1,0,0} };
       //Calling matrix array.
       int result = _adjacent.AdjacentElements(arr);
       PatientsResult obj = new PatientsResult();
       //Result Says number of groups.
       obj.numberOfGroups = result;
       return obj;
    }
}
