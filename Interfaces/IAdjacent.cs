using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Interfaces 
{
    public interface IAdjacent
    {
        int AdjacentElements<T>(T[,] arr);
    }
}